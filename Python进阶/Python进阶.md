#  Python 进阶

## 1、Python 面向对象编程

### 1.1、类的基础知识

与其他面对对象语言类似，`Python`也有类这个概念。

声明语法：

```python
class 类名:
    """类的说明文档"""
    
    def __init__(self):
        # 构造函数逻辑
        
    其他类方法或类属性
```

>**注意**：
>
>`Python` 中 **方法** 第一个参数必须为`self`，`self` 与 其他面对对象语言中的`this`类似，`self`是用于 **指向类构建出的实例对象**，而并非指向 **类结构本身对象**



- 使用`type()`方法可以查看传入的参数所属的 **类**

  ```python
  # <class 'int'>
  print(type(123))
  # <class 'str'>
  print(type('123'))
  ```

- 使用`dir()`方法可以查看**传入对象内部的属性与方法**



### 1.2、类的继承与重写

同其他面对对象语言一致，`Python`中的类也可以实现继承，与 `Java` 不同的是，`Python` 和 `C++` 一致，允许实现多继承。

实现语法：

```python
class 类名(继承类列表):
    类逻辑
    
# 单继承
class 类名(继承父类名):
    类逻辑
    
# 多继承
class 类名(继承父类名1, 继承父类名2):
    类逻辑
```



同其他面对对象语言一致，也允许 **方法的重写** 子类方法名和父类一致时即可实现对父类方法的重写。

```python
class Parent:
    def myMethod(self):
        print('调用父类方法')
        
class Child(Parent):
    def myMethod(self):
        print('调用子类方法')
        
        
if __name__ == '__main__':
    c = Child()		# 子类实例
    c.myMethod()	# 子类调用重写方法
    # 用子类对象调用父类已被覆盖的方法
    super(Child, c).myMethod()
```



在`Python`中，子类中若想要访问父类的属性与方法，可以使用`super`关键字，例如：

```python
# 子类调用父类方法
super(子类名, 实例参数名).父类方法或属性

# 子类调用父类构造函数
super(子类名, self).__init__(参数1，参数2)
```

> 注意：
>
> 在多继承的时候，通过子类调用父类构造方法时，调用的时父类列表中的第一个父类的构造方法



### 1.3、类的私有属性与方法

- **类的私有属性**：

  声明语法：`self.__属性名`

  两下划线开头，声明该属性为私有属性，不能在类的外部被使用或直接访问

- **类的私有方法**：

  声明语法：`self.__方法名`

  两下划线开头，声明该方法为私有属性，不能在类的外部被使用或直接访问



### 1.4、类的静态方法与类方法

- **类的静态方法**：

  `staticmethod 方法`一般是指自己独立，不依靠类中其他任何属性和方法的方法。

  `staticmethod 方法`，类的静态方法，可以实现直接通过`类.静态方法名()`的方式直接调用，也可以通过`实例对象.静态方法名()`的方式调用。

  ```python
  class 类名(继承类列表):
      
      @staticmethod
      def 方法名(参数列表):
          函数逻辑
  ```

- **类的类方法**：

  `classmethod 方法` 一般是指需要依赖类中其他属性或方法的方法。

  `classmethod 方法`，对应的函数不需要实例化，**不需要`self`参数，但第一个参数需要时表示自身类的`cls`**，可以用来调用 **类的属性**，**类的方法**，**实例化对象** 等

  ```python
  class 类名(继承类列表):
      
      @classmethod
      def 方法名(cls, 参数列表):
          函数逻辑
  ```



### 1.5、类的内置属性和方法

#### 类的内置属性：

- `__dict__`

  字典描述的类信息

- `__doc__`

  类的文档字符串

- `__name__`

  类名

- `__module__`

  类定义所在的模块

- `__bases__`

  类的所有父类构成元素

#### 类的内置方法：

- `__init__()`

  类的构造方法，生成对象时调用

- `__repr__()`

  打印，转换

- `__str__()`

  类的字符串描述

  ```python
  class Test:
      
      def __init(self, name):
          self.name = name
      
      def __str__(self):
          return self.name
      
  # '这是自定义描述'
  print(Test('这是自定义描述'))
  ```

- `__del__()`

  类的析构方法，释放对象时调用

- `__setitem__()`

  按照索引赋值

- `__getitem__()`

  按照索引获取值

- `__cmp__()`

  用于 **一个或多个类生成的实例对象之间** 的比较运算

- `__call__()`

  类的函数形式调用

- `__add__()`

  用于 **一个或多个类生成的实例对象之间** 的加法运算

  ```python
  class Vector:
      
      def __init(self, x, y):
          self.x = x
          self.y = y
      
      # other 是关键字, 表示另外一个对象
      def __add__(self, other):
          return Vector(self.x + other.x, self.y + other.x)
      
      
  vector1 = Vector(2, 3)
  vector2 = Vector(4, 5)
  # 
  print(vector1 + vector2)
  ```

- `__sub__()`

  用于 **一个或多个类生成的实例对象之间** 的减法运算

- `__mul__()`

  用于 **一个或多个类生成的实例对象之间** 的乘法运算

- `__truediv__()`

  用于 **一个或多个类生成的实例对象之间** 的除法运算

- `__mod__()`

  用于 **一个或多个类生成的实例对象之间** 的求余运算

- `__pow__()`

  用于 **一个或多个类生成的实例对象之间** 的乘方运算

  



## 2、Python 装饰器

### 2.1、装饰器函数

#### 2.1.1、装饰器函数使用场景

![装饰器函数使用场景](装饰器函数使用场景.png)

- 场景：希望在一个函数 **执行前** 或 **执行后** 进行一定的操作，并能够实现统一的调度与封装



**场景** 解决思路：

> **注意**：
>
> 这里的`hi`其实是`pi`，只是`PPT`打错了而已

![装饰器函数使用场景一解决思路](装饰器函数使用场景一解决思路.png)

由该解决思路可以引出`Python`装饰器函数：

使用`@函数名`声明 **某个函数是作为装饰器函数的参数传入装饰器函数**

![装饰器函数语法糖](装饰器函数语法糖.png)



#### 2.1.2、函数参数的传递

- 可以将函数的运行结果赋给另一个函数

  ```python
  def p1(name="python"):
      return 'p1' + name
  
  # 'p1 python'
  print(p1())
  ```

- 可以将函数名赋给另一个变量

  ```python
  greet = p1
  
  # 'p1 python'
  print(greet())
  ```

- 在`Python`中 **删除函数** 仅删除变量，而并非删除 **函数本身**

  ```python
  del p1
  
  # 抛出异常 NameError
  print(p1())
  
  # 'p1 python'
  print(greet())
  ```

- 在函数中定义函数

  调用`auth()`时，`p1()`和`welcome()`将会同时被调用，但是在`auth`之外无法访问`p1`和`welcome`，可以说`p1`和`welcome`是`auth`的私有函数

  ```python
  def auth(name="python"):
      print("now you are inside the auth() function")
      
      def p1():
          return "now you are in the p1() function"
      
      def welcome():
          return "now you are in the welcome() function"
      
      # "now you are in the p1() function"
      print(p1())
      print("auth")
      
  # "now you are in the p1() function"
  # "auth"
  auth()
  
  # 抛出异常 NameError
  print(p1())
  ```

- 函数返回函数

  ```python
  def auth(name="python"):
      def p1():
          return "now you are in the p1() function"
      
      def welcome():
          return "now you are in the welcome() function"
      
      if name == "python":
          # 将函数名作为返回值返回
          return p1
      else:
          return welcome
      
  a = auth()
  
  # <function p1 at 0x00000>
  print(a)
  
  # "now you are in the p1() function"
  print(a())
  ```

- 函数作为参数传递

  ```python
  def p1():
      reyurn "p1 python"
      
  def fun(func):
      print("i am doing something before p1()")
      print(func())
      
  # "i am doing something before p1()"
  # "p1 python"
  fun(p1)
  ```



#### 2.1.3、装饰器的四大问题及解决思路

- 若 **装饰器装饰的函数** 有返回值怎么办？

  **解决思路**：可以在 **装饰器函数内的子函数** 定义返回值

  ![装饰器四大问题之被监视函数有返回值问题](装饰器四大问题之被监视函数有返回值问题.png)

- 若 **装饰器装饰的函数** 有参数怎么办？

  **解决思路**：可以在 **装饰器函数内的子函数** 定义参数

  ![装饰器四大问题之被监视函数有参数问题](装饰器四大问题之被监视函数有参数问题.png)

- 若 **装饰器存在多层嵌套的情况** 怎么办？

  **解决方法**：`@函数名`允许一个函数被多个装饰器函数装饰，此外还可以在装饰器函数内部使用`wraps`函数来执行复制函数名称，注释文档，参数列表等功能，`wraps`函数依赖`functools`模块。

  ![装饰器四大问题之装饰器多层嵌套问题](装饰器四大问题之装饰器多层嵌套问题.png)

- 若 **装饰器函数** 本身也带参数怎么办？

  **解决方法**：将 **装饰器函数** 作为一个 **函数的子函数**，然后将参数传入 **装饰器函数的父函数** 并在 **装饰器修饰符修改为装饰器的父函数名并添加参数** 即可。

  ![装饰器四大问题之装饰器函数带参问题](装饰器四大问题之装饰器函数带参问题.png)



### 2.2、装饰器类

类也可以作为装饰器，实现手段是重写类的`__call__`方法。

**使用示例**：

![类装饰器](类装饰器.png)



## 3、Python 异常处理

与其他语言的`try...catch...finally...`组成的异常处理不同，`Python`中异常处理是由`try...except...finally...`

>**注意**：
>
>在`Python`中，所有异常的基类为`BaseException`



### 3.1、`try...except...`异常处理的简单流程

![python异常处理简单流程](python异常处理简单流程.png)

> **注意**：
>
> 一个`try...except`结构也可以包含多个`except`子句，一个`except`子句可以同时处理多个异常，这些异常将被放在一个括号内成为一个元组传入。
>
> ```python
> try:
>     pass
> except RuntimeError:
>     pass
> except(TypeError, NameError):
>     pass
> ```



### 3.2、Python 异常处理中的`else`语句

在`Python`的异常处理中，`else`子句表示在`try`**没有抛出异常时执行的子句**。

所有的`else`子句必须放置于`except`子句后。

![python异常处理中的else子句](python异常处理中的else子句.png)



### 3.3、Python 异常处理中的`finally`语句

在`Python`的异常处理中，`finally`子句表示在 **异常处理结束后执行的子句**。

所有的`finally`子句必须放置于整个异常处理结构的最后。

![python异常处理中的finally子句](python异常处理中的finally子句.png)



### 3.4、主动抛出异常

在`Python`中，允许使用`raise 异常类`主动抛出指定类型的异常。

![python主动抛出异常](python主动抛出异常.png)



## 4、Python 常用工具

### 4.1、Python 静态代码检测工具 - Flake8

![python 静态代码检查工具 Flack8](python 静态代码检查工具 Flack8.png)

**官方文档**：https://flake8.pycqa.org/en/latest/index.html



### 4.2、Python 第三方库 requests

主要用于处理 HTTP 请求

![python 第三方库 requests](python 第三方库 requests.png)

**官方网址**：http://docs.python-requests.org/zh_CN/laest/user/quickstart.html



### 4.3、Python 第三方库 xlrd, xlwt

`xlrd`主要用来读取`excel`文件，`xlwt`主要用来写`excel`文件

![python 第三方库 xlrd, xlwt](python 第三方库 xlrd, xlwt.png)

**官方网址**：http://www.python-excel.org/



### 4.4、Python 第三方库 shutil

主要用于文件的移动，复制，打包，压缩与解压

![python 第三方库 shutil](python 第三方库 shutil.png)



# 课堂练习

## 1、定义一个数字计数器类

### 要求：

定义一个数字计算器类，提供：

- 构造方法中可以设置开始计时的时刻：`xx时，xx分，xx秒`
- `run`方法能够按秒的`60`进制单位自动增加
- `show`方法返回当前计时的字符串表示，如：`01:10:58`
- 创建主函数`main`，使用`while`循环让时钟每一秒打印一次计时



### 实现：

```python
from time import sleep


class Timer:
    """
    计时器
    """

    def __init__(self, hour=0, minute=0, second=0):
        self.hour = hour
        self.minute = minute
        self.second = second

        # 检测数据格式
        self.__check()

    def __check(self):
        self.__check_small()
        self.__check_big()

    def __check_small(self):
        if self.second < 0:
            self.second = 0

        if self.minute < 0:
            self.minute = 0

        if self.hour < 0:
            self.hour = 0

    def __check_big(self):
        if self.second >= 60:
            self.second -= 60
            self.minute += 1

        if self.minute >= 60:
            self.minute -= 60
            self.hour += 1

        if self.hour >= 24:
            self.hour -= 24

    def run(self):
        self.second += 1

        self.__check_big()

    def show(self):
        return "现在时间为：%02d:%02d:%02d" % (self.hour, self.minute, self.second)


if __name__ == '__main__':
    timer = Timer()

    for time in range(60):
        timer.run()
        print(timer.show())

        sleep(1)

```



## 2、定义一个类描述平面上的点

### 要求：

定义一个类描述平面上的点，提供：

- 构造方法中设置点的初始坐标
- `move_to`方法让点移动到目标位置
- `dis_to`方法计算该点到目标点的距离
- 重写`__str__`方法描述当前点的信息



### 实现：

```python
class Point:
    """
    描述平面上点
    """

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f"当前点位于：（x: { self.x }, y: { self.y }）"

    def move_to(self, x, y):
        self.x = x
        self.y = y

    def dis_to(self, x, y):
        return round(((self.x - x) ** 2 + (self.y - y) ** 2) ** 0.5, 3)


if __name__ == '__main__':
    point = Point(1, 2)
    print(point)

    print(point.dis_to(5, 4))

    point.move_to(5, 4)
    print(point)

```



## 3、实现记录日志装饰器

### 要求：

定义一个记录日志的装饰器，要求在被装饰函数运行之前在屏幕上打印运行函数的名称，并记录当前系统时间

### 解答：

```python
import datetime
from functools import wraps

def log_it(func):
    @wraps(func)
    def with_logging(*args, **kwargs):
        print(func.__name__, "was called at", datetime.datetime.now())
        
        return func(*args, **kwargs)
    
    return with_logging


@log_it
def a_function(name)：
	return f"my name is : { name }"


a_function('jack')
```



## 4、实现异常处理装饰器

### 要求：

定义一个异常处理装饰器，对被装饰函数自动进行异常处理，并打印出异常原因

> **注意**：
>
> 在`Python`中，所有异常的基类为`BaseException`

### 解答：

```python
from functools import wraps


def exp_test(func):
    @wraps(func)
    def try_function(*args, **kwargs):
        result = 0
        
        try:
            result = func(*args, **kwargs)
        except BaseException as err:
            print("function has an error")
            print(err)
        else:
            print("function goes well")
            return result
        finally:
            print("function end with logs")
    
    return try_function
    


@exp_test
def a_function(num)：
	return 1 / num


print(a_function(0))
print(a_function(1))

```



# 课后练习

## 1、Python 类的编写与继承

### 要求：

编写以下类，并创建`Student`和`CollegeStudent`对象进行测试，让子类调用自身和父类的方法。

- 编写`Person`，具有以下属性与方法
  - 姓名`name`
  - 年龄`age`
  - `speak()` - 打印出当前`Person`对象的姓名和年龄
- 编写`Student`类继承`Person`类，具有以下属性和方法
  - 姓名`name`
  - 年龄`age`
  - 学校`school`
  - 学号`my_id`
  - `speak()` - 打印出当前`Student`对象所有属性信息
  - `run(b, times)` - 设置学生速度和时间，输出学生跑步距离
- 编写`CollegeStudent`类继承`Student`类，具有以下属性和方法
  - 姓名`name`
  - 年龄`age`
  - 学校`school`
  - 学号`my_id`
  - 语言`language`
  - `speak()` - 打印出当前`CollegeStudent`对象的所有属性信息
  - `coding` - 使用10秒打印出`I am coding ...(十个点)`

### 解答：

```python
from time import sleep


class Person:
    """
    人
    """

    def __init__(self, name, age):
        """
        构造函数
        """
        self.name = name
        self.age = age

    def speak(self):
        """
        打印出当前 Person 对象的姓名和年龄
        """
        return f"我的名字是{ self.name }，今年{ self.age }岁\n"


class Student(Person):
    """
    学生
    """

    def __init__(self, name, age, school, my_id):
        """
        构造函数
        """
        super(Student, self).__init__(name, age)

        self.school = school
        self.my_id = my_id

    def speak(self):
        """打印出当前`Student`对象所有属性信息"""
        return super(Student, self).speak() + f"我的来自{ self.school }，学号为{ self.my_id }\n"

    def run(self, b, times):
        """
        设置学生速度和时间，输出学生跑步距离
        """
        print(f"学生的速度为：{ b }, 跑步时间为：{ times }秒, 共跑了：{ b * times }米")


class CollegeStudent(Student):
    """
    大学生
    """

    def __init__(self, name, age, school, my_id, language):
        """
        构造函数
        """
        super(CollegeStudent, self).__init__(name, age, school, my_id)

        self.language = language

    def speak(self):
        """
        打印出当前`CollegeStudent`对象的所有属性信息
        """
        return super(CollegeStudent, self).speak() + f"我使用的语言为{ self.language }\n"

    def coding(self):
        """
        敲代码
        """
        print('I am coding', end='')

        for time in range(10):
            print('.', end='')
            sleep(1)


if __name__ == '__main__':
    lky = CollegeStudent('兰可易', 21, '东华理工大学', '201820620210', 'Python')

    print(lky.speak())

    lky.coding()

```



## 2、定义一个授权装饰器函数并测试其功能

### 要求：

- 编写⼀个 `AuthClass` 类封装 `name` 和 `pwd` 属性，对应⽤户名和密码类中拥有授权⽅法 `auth()`，判断当前对象的 `name` 值是否为 `python` , `pwd` 值是否为 `123` ，如果是返回 `True` ，否则返回`False` 。

- 基于 `AuthClass` 创建⼀个⽤户对象 `user` ，创建时填⼊⽤户名和密码

- 编写装饰器函数，⾸先让 `user` 对象来验证身份，如果验证通过，运⾏被装饰的函数，否则打印认证失败，返回 `False`

### 解答：

```python
```



## 3、定义一个带文件名参数的记录日志装饰器函数并测试其功能

### 要求：

- 在被装饰函数运⾏之前，在屏幕打印运⾏函数的名称，并记录当前系统时间

- 给装饰器函数增加输⼊⽇志⽂件名参数，在⽇志记录时将⽇志信息写⼊到对应的⽇志⽂件

### 解答：

```python
```

