import time
import turtle


def translate(cipher):
    """"
    翻译函数
    """
    translateDict = {
        'y': 'a', 'Y': 'A',
        'z': 'b', 'Z': 'B'
    }

    # 生成翻译字典
    for char in range(24):
        translateDict[chr(char + 65)] = chr(char + 67)
        translateDict[chr(char + 97)] = chr(char + 99)

    # 翻译
    reStr = ''

    for char in cipher:
        if char in translateDict.keys():
            reStr += translateDict[char]
        else:
            reStr += char

    return reStr


def toDict():
    """
    转字典函数
    """
    reList = []
    dictTitle = [
        'brand', 'color', 'size'
    ]

    with open('shoes.txt', 'r') as shoesFile:
        lines = shoesFile.read().splitlines()

        for line in lines:
            goodDict = {}
            goodDataList = line.split(' ')

            for index in range(len(dictTitle)):
                goodDict[dictTitle[index]] = goodDataList[index]

            reList.append(goodDict)

    return reList


def fac(number):
    """
    阶乘函数
    """
    reNum = 1

    while number > 0:
        reNum *= number
        number -= 1

    return reNum


def draw():
    """
    绘画函数
    """
    pen = turtle.Pen()
    pen.pencolor('blue')
    pen.pensize('4')

    for x in range(1, 6):
        pen.forward(100)
        pen.left(216)


def inputStatistics():
    """
    输入统计
    """
    statisticsDict = {
        'char': 0,
        'num': 0
    }

    inputStr = input()

    for char in inputStr:
        if char.isalpha():
            # 字母
            statisticsDict['char'] += 1
        elif char.isdigit():
            # 数字
            statisticsDict['num'] += 1

    print(f"{statisticsDict['char']} 个字母， {statisticsDict['num']} 个数字")


def identical():
    """
    判断输入的两个单词是否相同
    """
    inputDict = {
        'firstStr': [],
        'secondStr': []
    }

    print('请输入第一个单词: ')
    inputDict['firstStr'] = list(input())
    print('请输入第二个单词: ')
    inputDict['secondStr'] = list(input())

    if len(inputDict['firstStr']) != len(inputDict['secondStr']):
        return False

    inputDict['secondStr'].sort()
    inputDict['secondStr'].sort()

    if inputDict['firstStr'] == inputDict['secondStr']:
        return True
    else:
        return False


class Distance:
    """
    课后练习三
    """
    robotPosition = {
        'x': 0,
        'y': 0
    }

    def __init__(self):
        pass

    def calculation(self, order, num):
        """
        计算
        """
        num = int(num)
        order = order.upper()

        if order == 'UP':
            self.robotPosition['y'] += num
        elif order == 'DOWN':
            self.robotPosition['y'] -= num
        elif order == 'RIGHT':
            self.robotPosition['x'] += num
        elif order == 'LEFT':
            self.robotPosition['x'] -= num
        else:
            print('指令错误，请重新输入')

    def pythagorean(self):
        return round((self.robotPosition['x'] ** 2 + self.robotPosition['y'] ** 2) ** 0.5)

    def distance(self):
        """
        计算机器人距离
        """
        while True:
            print('请依此输入指令与数字，若需要停止则输入STOP')
            strList = input().split(' ')
            if len(strList) == 2:
                self.calculation(*strList)
            elif len(strList) == 1 and strList[0].upper() == 'STOP':
                break
            else:
                print('输入指令有误，请输入正确指令，若需结束则输入STOP')

        return self.pythagorean()


if __name__ == '__main__':
    # 课堂练习一
    # cipherText = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq " \
    #              "glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw " \
    #              "ml rfc spj. "

    # print(translate(cipherText))

    # 课堂练习二
    # print(toDict())

    # 课堂练习三
    # for num in range(1, 11):
    #     startTime = time.time()
    #     re = fac(num)
    #     endTime = time.time()
    #
    #     print(f'{ num } 的阶乘为：{ re }, 共耗时：{ endTime - startTime} s')

    # 课堂练习四
    # draw()

    # 课后练习一
    # inputStatistics()

    # 课后练习二
    # print(identical())

    # 课后练习三
    myDistance = Distance()
    print('距离为: ', myDistance.distance())
