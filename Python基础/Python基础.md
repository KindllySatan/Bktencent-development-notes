# Python 基础



## 1、介绍

`Python`是一个 **动态强类型**、**通用型**、**解释型** 语言，`Python`中 **缩进有强制规范**，目的是为了让`Python`代码更加优雅与高可读性。

**`Python`文件结构示范：**

![Python代码示例](Python代码示例.png)



## 2、标识符（变量名）

### 命名规范

- 首字母必须为**英文字母**或者是**`_`**

- 除首字母外其他部分由**英文字母**，**数字**或**`_`**构成

- `Python`中 **大小写敏感**

- `Python3`允许使用`UTF-8`编码字符作为变量名（但不推荐）

- 不能与 **保留字** 冲突

  ```python
  # 保留字查询方式
  import keyword
  
  keyword.kwlist
  ```

  

### 命名规则

- 参考`Python`的`PEP8`规范，参考链接：https://blog.csdn.net/ratsniper/article/details/78954852
- 或者参考蓝鲸官网的命名规范，参考链接：http://bk.tencent.com/docs/document/5.1/19/4121（链接已失效，未找到新的相关资料）



## 3、Python 数据类型与控制流

对于`Python`而言，一切数据皆为对象。

### 3.1、数据类型对象与标准数据类型

#### 数据类型对象

- 数字型：`int`、`float`、`complex`
- 布尔型：`boolean`
- 字符型：`string`
- 序列型：`list`、`tuple`、`range`
- 映射型：`dict`
- 集合型：`set`
- 文件类型：`File`

#### `Python3`标准数据类型

在`Python`标准数据类型中分为 **不可变数据类型** 和 **可变数据类型**。

- 不可变数据（**类似于变量保存数据**）：

  改变变量的值时，并非直接改变变量指向值，而是重新生成一个对象，然后将新对象赋值给该变量。

  - `Number` - 数字类型
  - `String` - 字符串类型
  - `Tuple` - 元组

- 可变数据（**类似于变量保存引用地址**）：
  
  改变变量的值时，直接改变变量指向值。
  
  - `List` - 列表
  - `Dictionary` - 字典
  - `Set` - 集合



### 3.2、数据类型介绍

#### 3.2.1、数字类型

- `Python3`支持`int`、`float`、`bool`、`complex`

- 在`Python3`中整数(`int`)**不限制大小**

- 在`Python3`中浮点数(`float`)**最大位数不能超过17位有效数字**

- 除常见运算（加`+`、减`-`、乘`*`、除`/`、求余`%`、幂指数`**`）外，其他**常见数学计算函数**包含在`math`模块中

  ```python
  # 引入 math 模块
  import math
  
  math.sqrt(2)
  
  # 查看模块内置函数
  # 语法：dir(模块名)
  dir(math)
  
  # 查看指定模块中指定函数的使用方法
  # 语法：help(模块名.函数名)
  help(math.sqrt)
  ```

- `Python`内置对 **复数** 的计算，对复数处理的数学函数包含在`cmath`中

  ```python
  # 引入 cmath 模块
  import cmath
  
  1 + 3j	# 输出 1 + 3j
  
  # 输出(0.6153846153846154+0.07692307692307691j)
  (1 + 2j) * (2 + 3j)	
  
  cmath.sqrt(1 + 2j)
  ```

  

#### 3.2.2、布尔类型

值仅有`True`与`False`两种，注意 **首字母是大写的**。

逻辑判断符号为 `or`、`and`、`not`



#### 3.2.3、字符串类型

定义字符串语法：`s = 'ilovepython'`

`Python` 中字符串一经定义则不能再改变。



- **`Python`下标索引规则**

  - **从左到右**索引默认由`0`开始，最大范围为字符串长度 - 1

  - **从右到左**索引默认由`-1`开始，最大范围为字符串开头字符索引，即`-字符串长度`

    ![Python索引规则](Python索引规则.png)

- **利用下标及切片访问字符串数据**

  在`python`中可以通过**下标**直接访问字符串的**一定条件约束的内容**，例如：

  ```python
  s = 'ilovepython'
  
  s[0]	# i
  s[1:5]	# love
  s[-6:]	# python
  # :: 为逆向切片，结果会反序输出
  s[::-1]	# nohtypevoli s[::-1]等价于s[-1::-1]
  ```

- **字符串常用内置函数**

  - `startswith() / endswith()`

    判断字符串是否以指定 **子字符串** **开头 或 结尾**，返回`boolean`类型数据

    ```python
    s = 'ilovepython'
    
    print(s.startswith('ilo'))	# True
    print(s.endswith('on'))		# True
    ```

  - `count()`

    判断字符串中 指定 **子字符串出现的次数**，返回`int`类型数据

    ```python
    s = 'ilovepython'
    
    s.count('o')	# 2
    ```

  - `find() / index()`

    判断字符串中 指定 **子字符串首次出现的索引**，返回`int`类型数据

    ```python
    s = 'ilovepython'
    
    s.find('y')		# 6
    s.find('m')		# -1
    s.index('y')	# 6
    s.index('m')	# 抛出异常
    ```

  - `join()`

    将列表数据以 **某指定规则** 进行拼接成 **字符串数据**

    ```python
    # 'i love python'
    ' '.join(['i', 'love', 'python'])
    ```

  - `replace()`

    替换字符串中 **所有指定子字符串** 为 **指定替换内容**

    ```python
    s = '  i love python   '
    s.replace(' ', '*')	# '**i*love*python***'
    ```

  - `split()`

    将字符串 按 **某指定规则** 进行切分成 **列表数据**，无参则默认空格为切分规则

    ```python
    s = 'i *love *python'
    s.split()	# ['i', '*love', '*python']
    s.split('*')	# ['i ', 'love ', 'python']
    ```

  - `strip()`

    去除字符串中 **首尾** 空白

    ```python
    s = '  ilovepython    '
    s.strip()	# 'ilovepython'
    ```

- **字符串的格式化**

  - `%s` - 占位符，用于填充指定 **变量或常量数据**

    ```python
    # "I'm from Xi an."
    "I'm from %s."%("Xi an")
    
    city = "Xi an"
    # "I'm from Xi an."
    "I'm from %s."%(city)
    ```

  - `{变量名或为空}` - 占位符，用于填充指定 **变量或常量数据**

    ```python
    # "I'm from Xi an."
    "I'm from {}.".format("Xi an")
    
    # "I'm from Xi an."
    "I'm from { city }.".format(city = "Xi an")
    ```

  - `f'字符串'` - 字符串模板

    ```python
    name = 'Tom'
    age = 16
    
    # "my name is Tom, my age is 16."
    print(f'my name is { name }, my age is { age }')
    ```



#### 3.2.4、列表类型

定义语法：`a_list = ['python', 123]`

`Python`列表允许 **不同类型数据** 存储于 **同一列表**，也允许**嵌套 列表、字典**等数据解构，列表同字符串一样可以使用 **切片** 来处理内部数据。



- **列表常用方法**

  - `append()`

    向 **列表尾部追加一个数据**

    ```python
    fruits = ['apple', 'banana', 'orange']
    
    print(fruits.append('mongo'))	# 空（当没有返回值，python 会返回一个 None 对象作为返回值）
    print(fruits)	# ['apple', 'banana', 'orange', 'mongo']
    ```

  - `extend()`

    向 **列表尾部追加一堆数据**

    ```python
    fruits = ['apple', 'banana', 'orange']
    
    print(fruits.extend(['mongo', 'pear']))	# None（当没有返回值，python 会返回一个 None 对象作为返回值）
    print(fruits)	# ['apple', 'banana', 'orange', 'mongo', 'pear']
    ```

  - `insert()`

    在列表中 **指定索引** 插入 **指定数据**

    ```python
    fruits = ['apple', 'banana', 'orange']
    fruits.insert(2, "pear")
    
    # ['apple', 'banana', 'pear', 'orange']
    print(fruits)
    ```

  - `pop()`

    弹出列表**尾部**元素，若指定索引，则弹出对应索引元素

    ```python
    fruits = ['apple', 'banana', 'orange']
    
    print(fruits.pop())	# 'orange'
    print(fruits)		# ['apple', 'banana']
    print(fruits.pop(0))	# 'apple'
    print(fruits)			# ['banana']
    ```

    

#### 3.2.5、元组类型

与 **列表类型** 相似，但略显区别，差异点有以下：

- 元组**一经定义则不可修改（除非是修改内部的引用类型数据来间接修改）**，列表可以修改
- 当元组仅包含`0`或`1`个元素时遵循特殊语法规则



元组定义语法：

```python
seq = ('python', 1977)
# 等价于
seq = 'python', 1977

# 当元组只有一项数据时，必须加上 , 声明元组
# 否则会被当作一个单独的其他类型
seq1 = ('python',)	# ('python')
seq1 = ('python')	# 'python'
```



#### 3.2.6、序列类型

`string`、`list`、`tuple`都属于`sequence`（序列）

**序列** 可以用于进行控制流的控制，例如`for..in`循环的条件

在控制流的过程中，若额外定义了变量`index`，则可以将 **序列** 转化为 **迭代器**，从而实现对循环控制流的准确索引

```python
a_list = ['a', 'b']
# enumerate 第二个参数为 index 的起始数, 为空则默认为0
for index, char in enumerate(a_list, 2):
    print((index, char), end=" ")
# 输出 (2, 'a') (3, 'b')
```



#### 3.2.7、字典类型

在`Python`中，对于存在**键 - 值**对应的对象，可以使用字典类型进行描述，字典是`Python`中除 **列表** 之外最为灵活的 **内置数据结构**。

> **字典的有序性**
>
> 在 `Python 3.7` 以前，字典的有序性并不作为`python`的语言特性存在，在`python 3.7及后续版本`，字典的有序性被作为特性确定下来。
>
> **有序性表现为**：
>
> 遍历时 **数据按照声明时数据顺序输出**，且**长度大小可变**，字典**元素可以被任意的删减和修改**。

定义语法：

```python
a_dict = {
    'adc': 123,
    98.69: 37,
    (1, 2): 200
}
```



- **列表 与 字典 的区别**

  **字典当中的元素是通过键值对来进行存取**的，本质是**类哈希图**的数据结构，而**列表是通过偏移存取**的，本质是**类线性表**的数据结构。

- **字典的基本操作**

  - 取值：`a_dict['abc']`
  - 修改：`a_dict['abc'] = 321`
  - 删除：`del a_dict['abc']`

- **字典的特性**

  - 字典中不允许 **同一键名** 出现多次
  - 键名不可改变
  - 键可以使用 **字符串**、**数字** 或 **不包含可变数据的元组** 充当
  
  > 一个对象能否作为字典的 **键`Key`** 主要取决于这个对象是否包含`**__hash__`方法**。
  >
  > 在`python`自带类型中，只有 **列表`list`**、**字典`dict`**、**集合`set`**以及 **内部包含三种以上类型的元组`tuple`** 不能作为`key`，其他对象都能作为`key`。
  
- **字典的常用方法**

  - `get()`

    用于根据 **键名** 查找一个 **键值对** 是否存在于该字典中，第二参数用于指定在 **该键值对不存在于字典中时返回的数据**

    ```python
    '''
        等效于 d = {
    		'a': 1,
    		'b': 2,
    		'c': 3
        }
    '''
    d = dict(a = 1, b = 2, c = 3)
    
    # 2
    d.get('b', -1)
    
    # -1
    d.get('d', -1);
    ```

  - `key() / values() / items()`

    用于查找对应数据（键名`key()`，值名`values()`，键值对元组`items()`），返回 **列表类型**

    ```python
    d = dict(a = 1, b = 2, c = 3)
    
    # ['a', 'b', 'c']
    d.key()
    
    # [1, 2, 3]
    d.values()
    
    # [('a', 1), ('b', 2), ('c', 3)]
    d.items()
    ```

  - `pop()`

    依据 **键名** 弹出 **字典中指定的键值对**，不指定则默认弹出字典中**最后一个键值对**

    ```python
    d = dict(a = 1, b = 2, c = 3)
    
    # 1
    print(d.pop("a"))
    
    # 3
    print(d.pop())
    
    # { 'b': 2 }
    print(d)
    ```

  - `update()`

    采用指定数据 **拓展** 字典

    ```python
    d = dict(a = 1, b = 2, c = 3)
    d1 = dict([('d': 4), ('e': 5)])
    
    d.update(d1)
    # { 'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5 }
    print(d)
    
    d.update(('f': 6))
    # { 'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6 }
    print(d)
    ```

  - `clear()`

    清空字典数据

    ```python
    d = dict(a = 1, b = 2, c = 3)
    
    d.clear()
    # {}
    print(d)
    ```

    

#### 3.2.8、集合类型

在`Python`中，集合`set`是一个**无序且不重复的元素序列**

声明语法：

```python
# 注意 set 内数据不是以键值对形式存在
a_set = { 2, 'a', 132 }
# 等效于
a_set = set(2, 'a', 132)
```

>**注意1**：
>
>创建一个 **空集合** 时必须使用 `set()`创建

---

> **注意2**：
>
> 在对 **集合** 使用`update()`的时候，向其 **传入集合包裹的字符串** 和 **直接传入字符串** 是不一样的。
>
> `set1.update( { "字符串" } )`：将字符串添加到集合中，若重复则忽略。
>
> `set1.update( "字符串" )`：将字符串**先拆分为单个字符组成的集合**，然后一个个的添加进集合中，若重复则忽略。

---

> **注意3**：
>
> 对于是由 **字典或是集合** 组成的集合，使用`.pop()`方式删除元素，若不明确指定删除元素，将会随机删除集合中元素。
>
> 对于是由 列表或是元组 组成的集合，使用`.pop()`方式删除元素，若不明确指定删除元素，将会从**第一个元素**开始删除元素。

- **集合相关运算**

  - 交集

    ```python
    # { 0, 1, 2 }
    set1 = set(range(3))
    # { 2, 3, 4 }
    set2 = set(range(2, 5))
    
    # { 2 }
    print(s1 & s2)
    ```

  - 并集

    ```python
    # { 0, 1, 2 }
    set1 = set(range(3))
    # { 2, 3, 4 }
    set2 = set(range(2, 5))
    
    # { 0, 1, 2, 3, 4 }
    print(s1 | s2)
    ```

  - 差集

    ```python
    # { 0, 1, 2 }
    set1 = set(range(3))
    # { 2, 3, 4 }
    set2 = set(range(2, 5))
    
    # { 0, 1 }
    print(s1 - s2)
    ```

  - 对称差

    对称差就是 **先算两集合并集**，**再去除其重复的数据项**

    ```python
    # { 0, 1, 2 }
    set1 = set(range(3))
    # { 2, 3, 4 }
    set2 = set(range(2, 5))
    
    # { 0, 1, 3, 4 }
    print(s1 ^ s2)
    ```

    

#### 3.2.9、文件类型

在`Python`中，从外部打开的文件统一被视为 **文件类型数据**，例如：

```python
f = open("md5.txt", 'r')

# <open file 'md5.txt', mode 'r' at 0xxxxxx>
print(f)
```

第一参数为 **文件路径索引**，第二参数为 **文件打开方式**

> **注意**：
>
> 使用`with open(文件内容) as 文件别名:` 会自动在结束所有逻辑业务后关闭文件，无需自己显式声明`file.close()`来关闭文件，也无需显式声明`file.flush()`来清除缓冲区数据。

- 示例文件（`md5.txt`）

  ```txt
  i love python
  i love c++
  i love c
  i love javascript
  ```

- 文件类型常用属性

  - `name` 文件名

    ```python
    f = open("md5.txt", 'r')
    
    # md5.txt
    print(f.name)
    ```

  - `mode` 打开方式

    ```python
    f = open("md5.txt", 'r')
    
    # modes: r
    ```

- **文件类型常用方法**

  - `read()`

    若指定 数字参数，则从文件头开始计算，读取指定参数数量`byte`的数据，若不指定数字参数，则默认读取整个文件。

    ```python
    f = open("md5.txt", 'r')
    
    # i love pyt
    print(f.read(10))
    '''
    	i love python
        i love c++
        i love c
        i love javascript
    '''
    print(f.read())
    ```

  - `readline()`

    读取**一行数据**（直至读到换号符`\n`或文件终止符`\EOF`）

    ```python
    f = open("md5.txt", 'r')
    
    # i love python
    print(f.readline())
    ```

  - `readlines()`

    读取指定数量的**多行数据**（直至读到换号符`\n`或文件终止符`\EOF`），若未指定则默认按行进行分割，读取整个文件数据

    ```python
    f = open("md5.txt", 'r')
    
    '''
    	[
            i love python
            i love c++
    	]
    '''
    print(f.readlines(2))
    
    '''
    	[
    		i love python
            i love c++
            i love c
            i love javascript
    	]
    '''
    print(f.readlines())
    ```

  - `seek()`

    指定文件读取光标位置

    ```python
    f = open("md5.txt", 'r')
    
    f.seek(10)
    # hon
    print(f.read(3))
    ```

  - `write()`

    ```python
    # 使用 python 语法糖打开文件
    with open("md5.txt", 'a') as f:
    	f.write('\ni love c#')
    
    f = open("md5.txt", 'r')
    
    '''
    	i love python
        i love c++
        i love c
        i love javascript
        i love c#
    '''
    print(f.read())
    ```

  - `flush() / close()`

    `flush()`清除缓冲区数据，`close()`关闭文件



### 3.3、Python 控制流

#### 3.3.1、条件语句

- **普通判断**

  ```python
  if a > 0:
      print("a > 0")
  else:
      if a < 0:
          print("a < 0")
      else:
          print("a = 0")
  ```

- **多重条件判断**

  ```python
  if a > 0:
      print("a > 0")
  elif a < 0:
      print("a < 0")
  else:
      print("a = 0")
  ```

- 三元表达式

  ```python
  "偶数" if a % 2 == 0 else "单数"
  ```

  

#### 3.3.2、`for...in` 循环

`Python`中的`for`循环形式类似于其他语言中的`for...in`循环形式。

在除了其他语言共通的`for...in`规则之外，`Python`的`for...in`循环还有着`for...else...`的特殊语法规则：

- `for...else...的执行顺序为：当迭代对象完成所有迭代后，若存在else子句，则执行else子句中的逻辑代码（但当由break语句退出时的情况除外）`



#### 3.3.3、`while`循环

除了书写结构不同之外，`Python`的`while`与其他语言中的规则一致。



## 4、Python 函数、模块、包

### 4.1、函数

声明语法：

```python
def 函数名(参数列表):
    函数逻辑内容
```

调用语法：

```python
函数名(参数列表)
```

> **注意**：
>
> 若 **函数名 不指定`()`符号** 时，则不进行函数调用，其 **函数名** 作为`Python` 中一个特殊的对象进行处理
>
> ```python
> def sayHello(name): 
> 	print('Hello ', name)
> 
> # <function __main__.sayHello>
> print(sayHello)
> ```



#### 4.1.1、`Python` 函数参数传递

`Python`函数的参数传递分为两种情况：**不可变类型参数** 与 **可变类型参数**。

- **不可变类型参数**：类似于 **值传递**，传递的只是参数所指向的值，而非参数本身，针对 **形参** 的修改不会影响 **实参**。
- **可变类型参数**：类似于 **引用传递**，传递的是参数本身，针对 **形参** 的修改也会影响 **实参**。

> **注意**：
>
> `python`中一切皆为对象，从严格上来讲不能说是 **值传递** 或是 **引用传递**，只是其表现形式类似于 **值传递** 或是 **引用传递**。



#### 4.1.2、函数参数

- 必备参数

  ```python
  def func(p):
      函数逻辑
  ```

- 默认参数

  默认参数 必须 **置于必备参数后** 进行声明

  ```python
  def func(a, b = 1):
      函数逻辑
  ```

- 可变参数（元组 / 字典）

  可变参数 **必须置于参数列表结尾**，且该函数参数列表**不可同时包含默认参数**。

  **可变参数元组** 采用`*`前缀进行申明，**可变参数字典** 采用`**`前缀进行申明。

  对于传入的参数，若 **除必选参数外** 传入的 **非键值对** 形式的参数，将匹配于 **可变参数元组**。而以 **键值对** 形式的参数，将匹配于 **可变参数字典**。

  ```python
  def func(a, *args, **kwargs): 
      函数逻辑
  ```

  ![python函数可变参数](python函数可变参数.png)

  > **拓展**：
  >
  > `Python`中，使用 **拆包运算符号`前缀*`**，可将**列表，元组**数据拆解为 **一个个参数的序列**。
  >
  > ```python
  > args = (0, 10, 2)
  > 
  > # [0, 2, 4, 6, 8]
  > print(list(range(*args)))
  > ```



#### 4.1.3、Python内置函数

![Python内置函数](Python内置函数.png)



#### 4.1.4、函数作用域

`Python`作用域解析顺序`Local -> Enclosing -> Global -> Built-in`

![python函数作用域例子](python函数作用域例子.png)



#### 4.1.5、匿名函数（`lambda`函数）

`Python`中匿名函数采用 `lambda`表达式 进行申明，匿名函数一般用于 **用之即弃** 的场景，例如：将函数作为参数传入函数。

对于 **匿名函数** 来说，若只有一个参数，则可以省略包裹参数列表的小括号，若只有一行函数逻辑代码，则可以省略包含函数逻辑体的大括号。

声明语法：`lambda 参数名: 函数逻辑`

```python
a = [0, 1, 2, 3, 4, 5]

# 将过滤规则函数和过滤原数据作为参数传入过滤器函数
result = filter(lambda x: x % 2 == 0, a)

# [0, 2, 4]
print(result)
```



#### 4.1.6、函数式编程

通过利用`Python`中 函数 **作为参数传入** 与 **作为返回值返回**（函数的叠加）的特性实现复杂的逻辑处理效果。

- **函数式编程** 相较 **面对对象编程** 的优点
  - **函数式编程** 能够简化很多设计模式
  - **函数式编程** 能够编写更多更为简单的函数，使之更易维护
  - **函数式编程** 更容易做单元测试
  - **函数式编程** 更容易跨线程，跨进程，甚至分布式执行任务



### 4.2、模块

导入模块语法：

- `import 模块名` 
- `import 模块名 as 别名` 
-  `from 模块名 import 模块内置函数名`
-  `from 模块名 import 模块内置函数名 as 别名`

在`Python`中，当一个 **模块(`module`) 作为整体被执行** 时，该模块的`__name__`的值为：`__main__`

当一个 **模块(`module`) 被其他`module`引用** 时，该模块的`__name__` 将为 **模块自己的名字**。



#### 4.2.1、Python 内置模块

![Python内置模块](Python内置模块.png)



#### 4.2.2、模块搜索路径规则

![python模块搜索路径规则](python模块搜索路径规则.png)



#### 4.2.3、Python 常用的内置模块及内置函数

`os` 模块：

![python os模块常用函数](python os模块常用函数.png)

`sys`模块：

![python sys模块常用函数](python sys模块常用函数.png)



#### 4.2.4、Python 常用的第三方模块

![常用的第三方模块](常用的第三方模块.png)



### 4.3、包

包 主要用于将一系列模块组成一个可方便管理的整体。

包 允许使用 `.` 来对 包 内部的模块进行分层，避免 **模块名污染问题**



# 随堂练习

## 1、字符串`ASCII`码转换

提供一串密文，知道其解密方式为 **密文字母所对应的`ASCII`码后移两位**，求明文。

```python
# 密文
ciphertext = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrg ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."
```

### 解答：

```python
# 课堂解法
def translate(ciphertext):
    '''
    	解密函数
    '''
    for char in ciphertext:
        if char >= 'a' and ch <= 'z':
            char = chr((ord(char) - ord('a') + 2) % 26 + ord('a'))
            print(char, end = '')
        else:
            print(char, end = '')
            
translate(ciphertext)
```

```python
# 自主解法
def translate(cipher):
    """"
    翻译函数
    """
    translateDict = {
        'y': 'a', 'Y': 'A',
        'z': 'b', 'Z': 'B'
    }

    # 生成翻译字典
    for char in range(24):
        translateDict[chr(char + 65)] = chr(char + 67)
        translateDict[chr(char + 97)] = chr(char + 99)

    # 翻译
    reStr = ''

    for char in cipher:
        if char in translateDict.keys():
            reStr += translateDict[char]
        else:
            reStr += char

    return reStr


if __name__ == '__main__':
    cipherText = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq " \
                 "glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw " \
                 "ml rfc spj. "

    print(translate(cipherText))

```

### 列表展开式解法：

吐槽：虽然老师说上面那种写法是新手才会写的格式，但是实在不能苟同老师说的这种利用列表展开式将所有逻辑挤在一行代码中执行的写法，代码是写给人看的而不是写给机器看的，这种写法可读性极差且极难进行维护修改。

```python
def translate(ciphertext):
    '''
    	解密函数 - 列表展开式写法
    '''
    print(''.join([chr((ord(char) - ord('a') + 2) % 26 + ord('a')) if char >= 'a' and char <= 'z' else char for char in ciphertext]))
    
translate(ciphertext)
```



## 2、读取文件信息并制成字典输出

![课堂练习2 读取文件信息并制成字典输出](课堂练习2 读取文件信息并制成字典输出.png)

### 解答：

```python
# 课堂解答
f = open('shoes.txt', r)
# 读取数据并去除每行数据前后的换行符及文件结尾符
lines = f.read().splitlines()
print(lines)

# 设定词典键名列表
title = ['brand', 'color', 'size']

rst = []	# 结果列表
for line in lines:
    d = {}
    
    # 根据空格切分结果值
    _line = line.split(' ')
    
    for i in range(len(title)):
        d[title[i]] = _line[i]
        
    res.append(d)
    
print(rst)
```

```python
# 自主解答
def toDict():
    """
    转字典函数
    """
    reList = []
    dictTitle = [
        'brand', 'color', 'size'
    ]

    with open('shoes.txt', 'r') as shoesFile:
        lines = shoesFile.read().splitlines()

        for line in lines:
            goodDict = {}
            goodDataList = line.split(' ')

            for index in range(len(dictTitle)):
                goodDict[dictTitle[index]] = goodDataList[index]

            reList.append(goodDict)

    return reList


if __name__ == '__main__':
    print(toDict())

```



## 3、根据需求编写模块并实现其功能

![课堂练习3 根据需求编写模块并实现功能](课堂练习3 根据需求编写模块并实现功能.png)

### 解答：

```python
import time


def fac(number):
    reNum = 1

    while number > 0:
        reNum *= number
        number -= 1

    return reNum


if __name__ == '__main__':
    for num in range(1, 101):
        startTime = time.time()
        re = fac(num)
        endTime = time.time()

        print(f'{ num } 的阶乘为：{ re }, 共耗时：{ endTime - startTime} s')
```



# 课后练习

## 1、统计输入字符串中字母和数字个数

### 要求：

编写 `Python` 代码统计输入的字符串中字母和数字的个数，并在最后输出统计结果

**输入**：`hello world! 123`

**输出**：`10`个字母，`3`个数字

### 解答：

```python
def inputStatistics():
    """
    输入统计
    """
    statisticsDict = {
        'char': 0,
        'num': 0
    }

    inputStr = input()

    for char in inputStr:
        if char.isalpha():
            # 字母
            statisticsDict['char'] += 1
        elif char.isdigit():
            # 数字
            statisticsDict['num'] += 1

    print(f"{ statisticsDict['char'] } 个字母， { statisticsDict['num'] } 个数字")
    
    
if __name__ == '__main__':
    inputStatistics()
```



## 2、判断输入的两段单词是否相同

### 要求：

编写 `Python` 代码判断用户输入的两个单词是否字母完全相同，且出现次数也相同

输入1：`anagram`、`nagaram`

输出1：`True`

输入1：`ana`、`nag`

输出1：`Flash`

### 解答：

```python
def identical():
    """
    判断输入的两个单词是否相同
    """
    inputDict = {
        'firstStr': [],
        'secondStr': []
    }

    print('请输入第一个单词: ')
    inputDict['firstStr'] = list(input())
    print('请输入第二个单词: ')
    inputDict['secondStr'] = list(input())

    if len(inputDict['firstStr']) != len(inputDict['secondStr']):
        return False

    inputDict['secondStr'].sort()
    inputDict['secondStr'].sort()

    if inputDict['firstStr'] == inputDict['secondStr']:
        return True
    else:
        return False
    
    
if __name__ == '__main__':
    print(identical())
```



## 3、计算机器人终点与原点距离

### 要求：

一个机器人从原点`(0, 0)`出发，可以进行上(`UP`)，下(`DOWN`)，左(`LEFT`)，右(`RIGHT`) 移动，移动方向由用户输入，输入`STOP`表示停止。最后输出机器人停止运动时距离原点的距离（当距离不是整数时，使用最接近的整数）

输入：

```python
UP 5
DOWN 3
LEFT 3
RIGHT 2
STOP
```

输出：`2`

### 解答：

```python
class Distance:
    """
    课后练习三
    """
    robotPosition = {
        'x': 0,
        'y': 0
    }

    def __init__(self):
        pass

    def calculation(self, order, num):
        """
        计算
        """
        num = int(num)
        order = order.upper()

        if order == 'UP':
            self.robotPosition['y'] += num
        elif order == 'DOWN':
            self.robotPosition['y'] -= num
        elif order == 'RIGHT':
            self.robotPosition['x'] += num
        elif order == 'LEFT':
            self.robotPosition['x'] -= num
        else:
            print('指令错误，请重新输入')

    def pythagorean(self):
        return round((self.robotPosition['x'] ** 2 + self.robotPosition['y'] ** 2) ** 0.5)

    def distance(self):
        """
        计算机器人距离
        """
        while True:
            print('请依此输入指令与数字，若需要停止则输入STOP')
            strList = input().split(' ')
            if len(strList) == 2:
                self.calculation(*strList)
            elif len(strList) == 1 and strList[0].upper() == 'STOP':
                break
            else:
                print('输入指令有误，请输入正确指令，若需结束则输入STOP')

        return self.pythagorean()
    
    
if __name__ == '__main__':
    myDistance = Distance()
    print('距离为: ', myDistance.distance())
```



# 拓展练习

## 1、文字解密

提供一串密文，知道解密对应方法为`k -> m`、`o -> a`，求明文。

```python
# 密文
ciphertext = "I'k LanKeYi, ky oge is 21"
```

### 解答：

```python
class Translate:
    """
        密文翻译类
    """
    passwordTable = {}
    reversePasswordTable = {}

    def __init__(self, passwordTable):
        self.passwordTable = passwordTable
        self.reversePasswordTable = dict(zip(
            passwordTable.values(),
            passwordTable.keys()
        ))

    def KTV_decrypt(self, cipher):
        """
            解密函数
        """
        newStr = ''

        for char in cipher:
            if char in self.passwordTable.keys():
                char = self.passwordTable[char]

            newStr += char

        return newStr

    def VTK_decrypt(self, cipher):
        """
            解密函数
        """
        newStr = ''

        for char in cipher:
            if char in self.reversePasswordTable.keys():
                char = self.reversePasswordTable[char]

            newStr += char

        return newStr


if __name__ == '__main__':
    # 译码表
    myPasswordTable = {
        'k': 'm',
        'o': 'a'
    }

    # 密文
    cipherText = "I'k LanKeYi, ky oge is 21"

    myTranslate = Translate(myPasswordTable)

    translationText = myTranslate.KTV_decrypt(cipherText)

    print(translationText)

```

